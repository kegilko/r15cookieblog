---
layout: page
title: "3D Printing"
meta: "false"
tags:
  - dev
---


# OpenSCAD 

- [OpenSCAD Coding Guidelines](/info/openscad-coding-guidelines)
- [Forum Post to Fix STLs using Meshlab](http://www.shapeways.com/forum/index.php?t=msg&th=1704&start=0&S=8acada7d3cfec6486dac50760db28646)

# 3D Rendering

The objective is to render STL into more realistic images.  Currently focused around PovRay as a tool


- [Models to be incorporated into PovRay](http://objects.povworld.org/cat)
- [Povray tutorial](http://www.f-lohmueller.de/pov_tut/pov__eng.htm)

# CNC

- [Interesting CNC Build using a wooden frame.](http://www.instructables.com/id/Arduino-CNC)  Also uses Arduino to control electronics
- [Easel](http://www.easel.com): Interested web-based CAD/CAM for 2.5D modelling.  Easy to use, and integrated into GRBL based controllers so that you can Mill directly from the web interface.
