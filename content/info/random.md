---
title: "Random"
draft: false
tags: [
    "random"
]
meta: "false"
---

# Random

Random links I wish to track, but can't not really an obvious catagory for.

- [myNoise](https://mynoise.net/): Background noices and interactive soundscapes.
- [After 45 Birthdays, Here are 12 Rules for Life](https://getpocket.com/explore/item/after-45-birthdays-here-are-12-rules-for-life)
- [I Want Simple, Not Just Easy](https://kristoff.it/blog/simple-not-just-easy/)
